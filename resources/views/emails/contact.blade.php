@component('mail::message')

Έχετε νέο μήνυμα από τη φόρμα επικοινωνίας του website σας Safeairpark<br><br>

 Ονοματεπώνυμο: {{ $mail_data['fullname'] }}<br>
 Email:  {{ $mail_data['email'] }}<br>
 Θέμα:  {{ $mail_data['subject'] }}<br>
 Μήνυμα: <br><br> 
{{$mail_data['message']}}
<br><br>
  


Thanks,

{{ config('app.name') }}
@endcomponent