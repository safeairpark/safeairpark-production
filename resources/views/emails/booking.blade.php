@component('mail::message')

Έχετε νέα κράτηση για το Safeairpark<br><br>

Αρ. Κράτησης: {{ $booking_data['booking_id'] }}<br><br>

 Σύνολο: {{ $booking_data['total'] }}€<br>

 Όνομα: {{ $booking_data['c_firstname'] }}<br>
 Επώνυμο: {{ $booking_data['c_lastname'] }}<br>
 Email:  {{ $booking_data['c_email'] }}<br>
 Τηλέφωνο:  {{ $booking_data['c_phone'] }}<br><br> 


 Χώρος:  {{ $booking_data['space'] }}<br>
 Μέρες:  {{ $booking_data['days'] }}<br>
 Από:  {{ $booking_data['date_from'] }}<br>
 Εως:  {{ $booking_data['date_to'] }}<br><br>

 Πινακίδα:  {{ $booking_data['car_plate'] }}<br>
 Μάρκα:  {{ $booking_data['car_make'] }}<br>
 Μοντέλο:  {{ $booking_data['car_model'] }}<br>

 Σημειώσεις:  <br><br>
 {{ $booking_data['c_notes'] }}<br>

Ευχαριστούμε,

{{ config('app.name') }}
@endcomponent