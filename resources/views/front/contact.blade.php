@extends('front.layouts.layout')

@section('extra-head')
 
    <title>Επικοινωνία | Safeairpark Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ"" name="keywords"> 
	
	
@endsection

@section('content')

<section class="py-5"><div class="container">
    <div class="row">
      <div class="col-12 col-lg-8 mx-auto text-center">
        <div class="row mb-5">
          <h2 class="fs-1 fw-bold fs-2">Επικοινωνία</h2>
          <p class="fs-4">Είμαστε στη διάθεσή σας για να σας εξυπηρετήσουμε όλο το 24ωρο</p>
        </div>
        <div class="row">
          <!-- Success message -->
            @if(Session::has('success'))
            <div class="alert alert-success">
                {{Session::get('success')}}
            </div>
          @endif
      </div>
        <div class="row">
          <div class="col-6 col-lg-4 mb-5">
            <span class="text-primary">
              <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 5a2 2 0 012-2h3.28a1 1 0 01.948.684l1.498 4.493a1 1 0 01-.502 1.21l-2.257 1.13a11.042 11.042 0 005.516 5.516l1.13-2.257a1 1 0 011.21-.502l4.493 1.498a1 1 0 01.684.949V19a2 2 0 01-2 2h-1C9.716 21 3 14.284 3 6V5z"></path></svg></span>
            <h6 class="mb-2 mt-3 text-muted fs-6">Τηλέφωνο</h6>
            <p class="mb-0"><a href="tel:+302114107010" class="text-decoration-none" style="color:#0a0001">+30 2114107010</a></p>
            <p class="mb-0"><a href="tel:+306932722710" class=" text-decoration-none " style="color:#0a0001">+30 6932722710</a></p>
            <p></p>
          </div>
          <div class="col-6 col-lg-4 mb-5">
            <span class="text-primary">
              <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path></svg></span>
            <h6 class="mb-2 mt-3 text-muted fs-6">E-mail</h6>
            <p class="mb-0">info@safeairpark.gr</p>
            <p></p>
          </div>
          <div class="col-12 col-lg-4 mb-5">
            <span class="text-primary">
              <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M17.657 16.657L13.414 20.9a1.998 1.998 0 01-2.827 0l-4.244-4.243a8 8 0 1111.314 0z"></path><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 11a3 3 0 11-6 0 3 3 0 016 0z"></path></svg></span>
            <h6 class="mb-2 mt-3 text-muted fs-6">Διεύθυνση</h6>
            <p class="mb-0">Λεωφ. Κορωπίου Αεροδρομίου</p>
            <p>Κορωπί Αττικής, 194 00</p>
          </div>
        </div>
      </div>
    </div>
    <form method="post" action="{{ route('front.contact.store') }}">
      @csrf
      <div class="row">
        <div class="col-12 col-lg-8 mx-auto">

          <div class="row">
            <div class="col-12 col-lg-6 mb-3">
                <div class="mb-3">
                    <input class="form-control py-2 px-3 bg-light border-0" name="subject" type="text" placeholder="Θέμα" value="{{old('subject')}}">
                    <span class="text-danger">{{ $errors->first('subject') }}</span>
                </div>
                <div class="mb-3">
                    <input class="form-control py-2 px-3 bg-light border-0" name="fullname" type="text" placeholder="Ονοματεπώνυμο" value="{{old('fullname')}}">
                    <span class="text-danger">{{ $errors->first('fullname') }}</span>
                </div>
                <div class="mb-3">
                    <input class="form-control py-2 px-3 bg-light border-0" name="email" type="email" placeholder="Email" value="{{old('email')}}">
                    <span class="text-danger">{{ $errors->first('email') }}</span>
                </div>

                
             
            </div>
            <div class="col-12 col-lg-6 mb-3">
              <textarea class="form-control py-2 px-3 bg-light border-0 h-100" name="message"  style="resize: none;" type="text" placeholder="Γράψτε το μήνυμά σας">{{old('message')}}</textarea>
              <span class="text-danger">{{ $errors->first('message') }}</span>
            </div>
          </div>

          <div class="row" style="margin-top: 20px;">
            <div class="col-12 col-lg-6 mb-3">
              <div class="form-check">
                <input class="form-check-input" type="checkbox" name="terms"  @if(old('terms')) {{"checked"}}@endif>
                  <label class="from-check-label" style="width:100%">Συμφωνώ με τούς <a href="{{ route('front.terms') }}" target="_blank">όρους χρήσης</a>.</label>
                  <span class="text-danger">{{ $errors->first('terms') }}</span>
  
              </div>
            </div>

            <div class="col-12 col-lg-6 mb-3">
              <div class="d-flex justify-content-between">
            
                <button class="btn btn-primary" type="submit">ΥΠΟΒΟΛΗ</button>
              </div>
    
            </div>
          </div>  

         
          
        </div>
      </div>
    </form>
  </div>
</section>
 
@include('front.partials.map')    
@include('front.partials.directions')

@endsection