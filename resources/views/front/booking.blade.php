
@extends('front.layouts.layout')

@section('before-head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css" integrity="sha512-ioRJH7yXnyX+7fXTQEKPULWkMn3CqMcapK0NNtCN8q//sW7ZeVFcbMJ9RvX99TwDg6P8rAH2IqUSt2TLab4Xmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />
<link rel="stylesheet" href="{{ asset('front/css/bootstrap-datepicker/bootstrap-datepicker.min.css') }}">

@endsection

@section('extra-head')
 
    <title>Booking | Safeairpark Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ" name="keywords"> 
	
@endsection

@section('content')



<section class="py-5 booking-section">
    <div class="container"> 
        <div class="row justify-content-center"> 
            <div class="col-12 col-md-8"> 
                <div class="card">
                    <h5 class="card-header">Eπιλέξτε ημερομηνίες</h5>
                    {{-- 
                        
                        <h5 class="card-title">Special title treatment</h5>
                        <p class="card-text">With supporting text below as a natural lead-in to additional content.</p>
                        <a href="#" class="btn btn-primary">Go somewhere</a>
                        --}}

                    <div class="card-body">
                        <form method="post" action="{{route('front.dates')}}" class="booking-form booking">
                        @csrf
                        <div class="row justify-content-center">

                             <!-- Success message -->
                            @if(Session::has('success'))
                                <div class="alert alert-success">
                                    {{Session::get('success')}}
                                </div>
                            @endif

                            <!-- Error  message -->
                            @if(Session::has('danger'))
                                <div class="alert alert-danger">
                                    {{Session::get('danger')}}
                                </div>
                            @endif

                        </div>


                        <div class="row justify-content-center"> 
                            <div class="form-group col-md-6 mb-4">
                                <label  class="required">Ημερομηνία Εισόδου <span class="asterisk">*</span></label>
              
                                <div class="input-group">
                                    <input type="text" class="form-control shadow-none datepicker" 
                                    id="datepicker_date_from" name="date_from" 
                                    value="
                                        @if(old('date_from'))
                                            {{old('date_from')}}
                                        @elseif(Session::get('booking.date_from'))
                                            {{Session::get('booking.date_from')}}
                                        @else
                                            {{date("d-m-Y H:i")}}
                                        @endif 
                                        ">
                                        <div class="input-group-append" >
                                            <span class="input-group-text" style="height: 38px;"><i class="fa fa-calendar"></i></span>
                                          </div>
                                </div>
                                @if ($errors->has('date_from'))
                                  <span class="text-danger">{{ $errors->first('date_from') }}</span>
                               @endif
                              <small  class="form-text text-muted">Επιλέξετε την ημερομηνία είσόδου.</small>
                            </div>

                            <div class="form-group col-md-6 mb-4">
                                <label  class="required">Ημερομηνία Εξόδου <span class="asterisk">*</span></label>
              
                                <div class="input-group">
                                    <input type="text" class="form-control shadow-none datepicker" 
                                    id="datepicker_date_to" name="date_to" 
                                    value="

                                        
                                        @if(old('date_to'))
                                            {{old('date_to')}}
                                        @elseif(Session::get('booking.date_to'))
                                            {{Session::get('booking.date_to')}}
                                        @else
                                            {{date("d-m-Y H:i")}}
                                        @endif 
                
                                        ">
                                      <div class="input-group-append" >
                                        <span class="input-group-text" style="height: 38px;"><i class="fa fa-calendar"></i></span>
                                      </div>
                                </div>
                                @if ($errors->has('date_to'))
                                  <span class="text-danger">{{ $errors->first('date_to') }}</span>
                               @endif
                              <small  class="form-text text-muted">Επιλέξετε την ημερομηνία εξόδου.</small>
                            </div>

                            <label  class="required">Χὠρος Οχημάτων <span class="asterisk">*</span></label>
                            <div class="form-group col-md-6 mb-4">
                                <select class="form-select" name="space" aria-label="Επιλέξτε όχημα">
                                    
                                    @foreach($spaces as $space)
                                    <option 
                                        value="{{$space->id}}" 
                                        @if(Session::get('booking.space_id') == $space->id || old('space') == $space->id) selected @endif>
                                        {{$space->label}}
                                    </option>
                                    @endforeach
                                </select>
                                @if ($errors->has('date_to'))
                                  <span class="text-danger">{{ $errors->first('space') }}</span>
                               @endif
                               <small  class="form-text text-muted">Επιλέξετε τον χώρο στάθμεσης.</small>
                            </div>

                            <div class="form-group col-md-6 mb-4">
                                <button type="submit" class="btn btn-primary">ΕΠΟΜΕΝΟ</button>
                            </div>
                        </div>
                    </form>

                    </div>
                </div>
            </div>
        </div> 
    </div> 
</section> 


@include('front.partials.newsletter')
    
@endsection


@section('before-footer')

<script  src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"  crossorigin="anonymous"> </script>
<script src="{{asset('front/js/moment/moment.min.js')}}"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/js/all.min.js" integrity="sha512-YfPUURC5CqzBHKXk0+zbKsE8BKmnt3TnW64tQkT0kShXIUU/ikFfCD0KPeXEE4cTCt1R/CNGLzjUDcN3AkYBRg==" crossorigin="anonymous" referrerpolicy="no-referrer"></script>
<script src="{{ asset('front/js/bootstrap-datepicker/bootstrap-datepicker.min.js') }}"></script>

@endsection


@section('after-footer')

<script>

    $(document).ready(function() {
    
    'use strict'

    $('.datepicker').datetimepicker({
    "allowInputToggle": true,
    "showClose": true,
    "showClear": true,
    "showTodayButton": true,
    "format": "DD-MM-YYYY HH:mm",
    "locale": "el",
    
    "icons": {
                time: 'fa fa-clock',
                date: 'fa fa-calendar',
                up: 'fa fa-chevron-up',
                down: 'fa fa-chevron-down',
                previous: 'fa fa-chevron-left',
                next: 'fa fa-chevron-right',
                today: 'fa fa-check',
                clear: 'fa fa-trash',
                close: 'fa fa-times'
            }
            
    
    });
});
</script>

@endsection