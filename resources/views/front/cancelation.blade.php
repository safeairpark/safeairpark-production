@extends('front.layouts.layout')

@section('extra-head')
 
    <title>Πολτική Ακυρώσεων | Safeairpark Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ"" name="keywords"> 
	
	
@endsection

@section('content')

<section class="py-5 simple-page">
    <div class="container">
      <div class="row">
        <div class="col-12 col-lg-8 mx-auto text-left">
            <div class="row mb-5">
              <h2 class="fs-1 fw-bold fs-2">Πολιτική Ακυρώσεων</h2>
              <p>
                Η Safeairpark σέβεται το χρόνο και τα χρήματά σας. Για το λόγο αυτό, έχουμε θεσπίσει μια ευέλικτη πολιτική ακυρώσεων.
              </p>
              <h4>Ακύρωση χωρίς προπληρωμή</h4>
              <p>
                Εάν ακυρώσετε μια κράτηση που δεν έχει προπληρωθεί, δεν θα σας χρεώσουμε. Μπορείτε να ακυρώσετε τη κράτηση σας ανά πάσα στιγμή, χωρίς να χρειάζεται να μας ενημερώσετε.
              </p>
              <h4>Ακύρωση με προπληρωμή</h4>
              <p>
                Εάν ακυρώσετε μια κράτηση που έχει προπληρωθεί, θα σας επιστρέψουμε τα χρήματά σας, εφόσον μας ενημερώσετε τουλάχιστον 24 ώρες πριν από την έναρξη της κράτησης. Εάν μας ενημερώσετε αργότερα από τις 24 ώρες, δεν θα σας επιστρέψουμε τα χρήματά σας.
              </p>
              <h4>Απελευθέρωση της θέσης</h4>
              <p>
                Σε περίπτωση που ακυρώσετε τη κράτησή σας, η θέση σας θα απελευθερωθεί για πώληση σε άλλους πελάτες.
              </p>
              <h4>Αργοπορία</h4>
              <p>
                Εάν δεν εμφανιστείτε στο χώρο στάθμευσης 30 λεπτά μετά την έναρξη της κράτησής σας, η κράτηση σας θα ακυρωθεί χωρίς την επιστροφή των χρημάτων.
              </p>
              <h4>Ερωτήσεις σχετικά με την πολιτική ακυρώσεων</h4>
              <p>
                Εάν έχετε ερωτήσεις σχετικά με την πολιτική ακυρώσεων, μπορείτε να επικοινωνήσετε μαζί μας στέλνοντας ένα email στη διεύθυνση [email protected]
              </p>
            </div>
        </div>
      </div>
    </div>
    
</section> 

 
@include('front.partials.newsletter')

@endsection