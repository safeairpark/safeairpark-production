<section class="" style="margin-top:15px;">
    <nav class="navbar navbar-expand-lg navbar-light"><div class="container">
      <a class="navbar-brand" href="{{ url('/')}}">
        <img src="{{ asset('front/images/logo/logo-dark.png')}}" alt="" width="140"></a>
      <button class="navbar-toggler" type="button" data-toggle="side-menu" data-target="#sideMenu05" aria-controls="sideMenu05" aria-expanded="false" aria-label="Toggle navigation">
        <span class="navbar-toggler-icon"></span>
      </button>
      <div class="collapse navbar-collapse" id="navbarExample">
        <ul class="ms-auto me-4 navbar-nav">
          <li class="nav-item me-4"><a class="nav-link" href="{{ url('/')}}">ΑΡΧΙΚΗ</a></li>
          <li class="nav-item me-4"><a class="nav-link" href="{{ route('front.booking')}}">ΚΡΑΤΗΣΗ</a></li>
          <li class="nav-item me-4"><a class="nav-link" href="{{ route('front.company')}}">Η ΕΤΑΙΡΕΙΑ</a></li>
          <li class="nav-item me-4"><a class="nav-link" href="{{ route('front.services')}}">ΥΠΗΡΕΣΙΕΣ</a></li>
          <li class="nav-item me-4"><a class="nav-link" href="{{ route('front.pricelist') }}">ΤΙΜΟΚΑΤΑΛΟΓΟΣ</a></li>
        </ul>
          <div>
            {{-- 
              <a class="btn btn-outline-primary me-2" href="#">ΣΥΝΔΕΣΗ</a>
              <a class="btn btn-primary" href="#">ΕΓΓΡΑΦΗ</a>
              --}}
              <a class="btn btn btn-primary" href="{{ route('front.contact')}}">ΕΠΙΚΟΙΝΩΝΙΑ</a>
              {{-- <a class="btn btn-outline-primary me-2" href="{{ route('front.contact')}}">ΕΠΙΚΟΙΝΩΝΙΑ</a>  --}}
          </div>
      </div>
    </div>
  </nav>
<div class="d-none fixed-top top-0 bottom-0" id="sideMenu05">
    <div class="position-absolute top-0 end-0 bottom-0 start-0 bg-dark" style="opacity: 0.7"></div>
    <nav class="navbar navbar-light position-absolute top-0 bottom-0 start-0 w-75 pt-3 pb-4 px-4 bg-white" style="overflow-y: auto;"><div class="d-flex flex-column w-100 h-100">
        <div class="d-flex justify-content-between mb-4">
          <a href="{{ url('/')}}">
            <img class="img-fluid" src="{{ asset('front/images/logo/logo-dark.png')}}" alt="" width="106"></a>
          <button class="btn-close" type="button" data-toggle="side-menu" data-target="#sideMenu05" aria-controls="sideMenu05" aria-label="Close"></button>
        </div>
        <div>
          <ul class="navbar-nav mb-4">
            <li class="nav-item"><a class="nav-link" href="{{ url('/')}}">ΑΡΧΙΚΗ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('front.booking')}}">ΚΡΑΤΗΣΗ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('front.company')}}">Η ΕΤΑΙΡΕΙΑ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('front.services')}}">ΥΠΗΡΕΣΙΕΣ</a></li>
            <li class="nav-item"><a class="nav-link" href="{{ route('front.pricelist')}}">ΤΙΜΟΚΑΤΑΛΟΓΟΣ</a></li>
            
          </ul>
          <div class="border-top pt-4 mb-5">
            {{-- 
              <a class="btn btn-outline-primary w-100 mb-2" href="#">Log in</a>
              <a class="btn btn-primary w-100" href="#">Sign up</a>
              --}}
              <a class="btn btn-primary w-100" href="{{ route('front.contact')}}">ΕΠΙΚΟΙΝΩΝΙΑ</a>
               {{-- <a class="btn btn-outline-primary w-100 mb-2" href="{{ route('front.contact')}}">ΕΠΙΚΟΙΝΩΝΙΑ</a> --}}
          </div>
        </div>
        {{-- 
        <div class="mt-auto">
          <p>
            <span>Get in Touch</span>
            <a href="#">info@safeairpark.gr</a>
          </p>
          <a class="me-2" href="#">
            <img src="metis-assets/icons/facebook-blue.svg" alt=""></a>
          <a class="me-2" href="#">
            <img src="metis-assets/icons/twitter-blue.svg" alt=""></a>
          <a class="me-2" href="#">
            <img src="metis-assets/icons/instagram-blue.svg" alt=""></a>
        </div>
        --}}
      </div>
    </nav>
</div>
</section>