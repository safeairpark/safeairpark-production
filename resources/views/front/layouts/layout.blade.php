<!DOCTYPE html>
<html lang="en">

<head>

    @yield ('before-head')

    @include ('front.layouts.head')

    @yield ('extra-head')

</head>

<body>

@include('front.layouts.header')

@yield ('content')

@include('front.layouts.footer')

@yield ('before-footer')

<!-- Vendor JS Files -->
<script src="{{ asset('front/js/bootstrap/bootstrap.bundle.min.js') }}"></script>

<!-- Template Main JS File -->
<script src="{{ asset('front/js/main.js') }}"></script>


@yield ('after-footer')

</body>

</html>