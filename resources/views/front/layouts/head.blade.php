<meta charset="utf-8">
<meta content="width=device-width, initial-scale=1.0, shrink-to-fit=no" name="viewport">

<!--
<title>{{ config('app.name') }}</title>
-->


<!-- Vendor CSS Files -->
<link rel="stylesheet" href="{{ asset('front/css/bootstrap/bootstrap.min.css') }}" rel="stylesheet">

 <!-- Google Fonts -->
<link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Roboto:300,400i,400,500,700&amp;subset=latin">
<link rel="stylesheet" href="{{ asset('front/css/main_106.css') }}" rel="stylesheet">

<link  rel="shortcut icon" type="image/vnd.microsoft.icon" href="{{ asset('front/images/logo/favicon.ico')}}">

  <!-- Favicons -->
  
 {{--  
    <link href="{{ asset('front/images/favicon.png') }}" type="image/png" sizes="32x32" rel="icon">
    <link href="{{ asset('front/images/apple-touch-icon.png') }}" rel="apple-touch-icon">
--}}

<!-- Google tag (gtag.js) -->
<script async src="https://www.googletagmanager.com/gtag/js?id=G-WR4RPN0GQD">
</script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'G-WR4RPN0GQD');
</script>