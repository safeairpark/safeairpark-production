@php $blob_image =asset('front/metis-assets/elements/blob.svg');@endphp
<section class="py-5" style="background-repeat: no-repeat; background-position: top; background-image: url('{{$blob_image}}');"><div class="container">
    <div class="position-relative py-5">
      <div class="position-absolute" style="top: 0; left: 0;">
        <img class="img-fluid" src="{{asset('front/metis-assets/elements/dots-left.svg')}}" alt=""></div>
      <div class="row">
        <div class="col-12 col-lg-7 py-5 mx-auto text-center">
          <h2 class="mb-3 fs-1 fw-bold">
            <span class="fs-2">Εγγραφή στο</span>
            <span class="text-primary fs-2">newsletter</span>
            <span class="fs-2">μας</span>
          </h2>
          <p class="text-muted fs-4">Εγγραφείτε στο newsletter για να λαμβάνετε ενημερώσεις και προσφορές για τις υπηρεσίες μας</p>
          <div class="row py-4 justify-content-center">
            <div class="col-12 col-md-6 mb-3 mb-md-0">
              <div class="input-group">
                <span class="input-group-text px-3 bg-white rounded-left">
                  <svg width="24" height="24" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M3 8l7.89 5.26a2 2 0 002.22 0L21 8M5 19h14a2 2 0 002-2V7a2 2 0 00-2-2H5a2 2 0 00-2 2v10a2 2 0 002 2z"></path></svg></span>
                <input class="form-control bg-white" type="text" placeholder="Γράψτε το e-mail σας"></div>
            </div>
            <div class="col-12 col-md-auto">
              <button class="w-100 btn btn-primary" type="submit">ΕΓΓΡΑΦΗ</button>
            </div>
          </div>
        </div>
      </div>
      <div class="position-absolute" style="bottom: 0; right: 0;">
        <img class="img-fluid" src="{{asset('front/metis-assets/elements/dots-right.svg')}}" alt=""></div>
    </div>
  </div>
</section>