<section class="py-5"><div class="container">
    <div class="row">
      <h2 class="fs-1 fw-bold fs-2"></h2>
      <div class="col-12 col-lg-6 mt-5 mt-lg-0 order-1 order-lg-0">
        <div class="mb-5 pe-5">
          <h2 class="mb-3 fw-bold fs-3">Από Αττική οδό</h2>
          <p class="lh-lg text-muted">Ακολουθήστε τις παρακάτω οδηγίες.</p>
        </div>
        <ul class="list-unstyled"><li class="mb-4">
            <span class="d-inline-block py-2 px-3 me-3 text-white rounded bg-primary">1</span>
            <span>Βγαίνετε στην ΕΞΟΔΟ 20 για ΚΟΡΩΠΙ</span>
          </li>
          <li class="mb-4">
            <span class="d-inline-block py-2 px-3 me-3 text-white rounded bg-primary">2</span>
            <span>Στο πρώτο φανάρι συνεχίζετε ευθεία προς ΚΟΡΩΠΙ</span>
          </li>
          <li>
            <span class="d-inline-block py-2 px-3 me-3 text-white rounded bg-primary">3</span>
            <span>Στα 100μ θα μας βρείτε στα δεξιά σας μέσα στον παράδρομο</span>
          </li>
        </ul></div>
      <div class="col-12 col-lg-6 mt-5 mt-lg-0 order-1 order-lg-0">
        <div class="mb-5 pe-5">
          <h2 class="mb-3 fw-bold fs-3">Από Βάρης - Κορωπίου</h2>
          <p class="lh-lg text-muted">Ακολουθήστε τις παρακάτω οδηγίες.</p>
        </div>
        <ul class="list-unstyled"><li class="mb-4">
            <span class="d-inline-block py-2 px-3 me-3 text-white rounded bg-primary">1</span>
            <span>Τελειώνοντας την ΒΑΡΗΣ-ΚΟΡΩΠΙΟΥ στρίβετε δεξιά στο φανάρι με κατεύθυνση προς Αεροδρόμιο.</span>
          </li>
          <li class="mb-4">
            <span class="d-inline-block py-2 px-3 me-3 text-white rounded bg-primary">2</span>
            <span>Στο πρώτο φανάρι συνεχίζετε ευθεία και στο δεύτερο στρίβετε δεξιά στην Λεωφόρο Κορωπίου Αεροδρομίου.</span>
          </li>
          <li>
            <span class="d-inline-block py-2 px-3 me-3 text-white rounded bg-primary">3</span>
            <span>Θα μας δείτε στα αριστερά σας. Στο επόμενο φανάρι στρίβετε δεξιά και αμέσως αριστερά για να περάσετε απέναντι στον παράδρομο.</span>
          </li>
        </ul></div>
    </div>
  </div>
</section>