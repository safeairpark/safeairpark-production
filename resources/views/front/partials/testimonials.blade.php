@php $back_image = asset('front/metis-assets/backgrounds/intersect.svg');@endphp
<section class="py-5" style="background-size: contain; background-repeat: no-repeat; background-image: url('{{$back_image}}');"><div class="container">
    <div class="row mb-5">
      <div class="col-12 col-lg-5 mx-auto text-center">
        <!--
        <img style="color:#000" src="metis-assets/icons/quote.svg" alt="">
  -->
        <h2 class="fs-2 fw-bold">Γιατί οι πελάτες μας μας εμπιστεύονται</h2>
        <p class="text-muted mb-0 fs-4">Διαβάστε τι λένε οι πραγματικοί πελάτες μας για την υπηρεσία μας</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-lg-4 mb-4" style="opacity: 0.5;">
        <div class="p-4 bg-white shadow-sm rounded">
          <!--
          <img class="mb-3" src="metis-assets/icons/quote.svg" alt="">
            -->
          <p class="mb-3 text-muted">Άριστη εξυπηρέτηση! Σε μεταφερει βανακι στο αεροδρόμιο με τα πράγματα σου χωρίς άγχος και άνετα! Αντίστοιχα σε παραλαμβάνει κατ οπιν συνεννόησης! Τιμές λογικές για τις αντίστοιχες του αεροδρομίου!</p>
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{ asset('front/images/theme/sandra.webp')}}" alt=""><div class="ms-3">
              <h5 class="mb-2">Sandra D.</h5>
              <!-- <p class="text-primary"></p> -->
        </div>
      </div>
    </div>
  </div>
  <div class="col-12 col-lg-4">
    <div class="p-4 bg-white shadow-sm rounded mb-4">
      <!--
      <img class="mb-3" src="metis-assets/icons/quote.svg" alt="">
    -->
      <p class="mb-3 text-muted">Εξαιρετική υπηρεσία, πλησίον του αεροδρομίου , άψογη εξυπηρέτηση κι αμεσότατη ,από τον ευγενεστατο κ. Θανάση που με μετέφερε από και προς το αεροδρόμιο χωρίς αναμονή και σε χρόνο που ξεπέρασε τις προσδοκίες μου. Το συνιστώ ανεπιφύλακτα και θα το ξαναπροτιμησω.</p>
      <div class="d-flex">
        <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{ asset('front/images/theme/dimitra.avif')}}" alt=""><div class="ms-3">
          <h5 class="mb-1">Dimitra B.</h5>
          <!-- <p class="text-primary"></p> -->
        </div>
      </div>
    </div>
  </div>
  <div class="mb-5 col-12 col-lg-4 mb-4" style="opacity: 0.5;">
    <div class="p-4 bg-white shadow-sm rounded">
      <!--
      <img class="mb-3" src="metis-assets/icons/quote.svg" alt="">
        -->
      <p class="mb-3 text-muted">Το πιο οικονομικό που βρήκα (βέβαια μέσω άλλου σαιτ η τιμή). Στην εξυπηρέτηση άψογοι, δεν άργησαν να μας παραλάβουν, το αμάξι στα χιλιόμετρα που το άφησα και το προσωπικό ευγενικό.</p>
      <div class="d-flex">
        <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{ asset('front/images/theme/giannis.webp')}}" alt=""><div class="ms-3">
          <h5 class="mb-1">Giannis Κ.</h5>
          <!-- <p class="text-primary"></p> -->
        </div>
      </div>
    </div>
  </div>
  </div>
<!-- <div class="text-center"> <button class="btn d-inline-block p-1 me-1 rounded-circle bg-secondary"></button> <button class="btn d-inline-block p-1 me-1 rounded-circle bg-primary"></button> <button class="btn d-inline-block p-1 me-1 rounded-circle bg-secondary"></button> <button class="btn d-inline-block p-1 rounded-circle bg-secondary"></button> </div> -->
</div>
</section>