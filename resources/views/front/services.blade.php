@extends('front.layouts.layout')

@section('extra-head')
 
    <title>Υπηρεσίες | Safeairpark Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ" name="keywords"> 
	
@endsection


@section('content')

<section class="py-5 overflow-hidden">
    <div class="container py-5">
      <div class="row align-items-center">
        <div class="position-relative col-12 col-lg-6 order-last mt-5 mt-lg-0">
          <img class="position-relative mx-auto rounded w-100" style="z-index:10" src="https://images.unsplash.com/photo-1525925946496-8abb0ec054d3?crop=entropy&amp;cs=srgb&amp;fm=jpg&amp;ixid=M3wzMzIzMzB8MHwxfHNlYXJjaHwxMXx8dHJhdmVsZXIlMjBpbiUyMGFpcnBvcnR8ZW58MHwxfHx8MTcwMjkzMTA4N3ww&amp;ixlib=rb-4.0.3&amp;q=85&amp;w=1920" alt="">
          <img class="img-fluid position-absolute" width="160" height="160" style="top:0; left:0; margin-left: -48px; margin-top: -48px;" src="{{asset('front/metis-assets/elements/blob-tear.svg')}}" alt="">
          <img class="img-fluid position-absolute" width="160" height="160" style="right:0; bottom:0; margin-right: -48px; margin-bottom: -48px;" src="{{asset('front/metis-assets/elements/blob-tear.svg')}}" alt="">
        </div>
        <div class="col-12 col-lg-6 py-5">
          <div class="row">
            <div class="col-12 col-lg-8 mx-auto">
              <span class="badge rounded-pill bg-primary">Parking Αεροδρομίου</span>
              <h2 class="mt-3 mb-5 fs-1 fw-bold">Safeairpark</h2>
              <div class="d-flex mb-4 pb-1">
                <span class="me-4 text-primary">
                  <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 9l3 3-3 3m5 0h3M5 20h14a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                  </svg>
                </span>
                <div>
                  <h5>ΦΥΛΑΞΗ ΟΧΗΜΑΤΟΣ</h5>
                  <p class="mb-0 text-muted">Είμαστε στη διάθεσή σας όλο το 24ωρο, με συνεχή και φιλική εξυπηρέτηση. Ο χώρος μας φυλάσσεται συνεχώς και το όχημά σας είναι ασφαλές καθώς επιβλέπεται από ηλεκτρονικά μέσα αλλά και από το έμπειρο προσωπικό μας.</p>
                </div>
              </div>
              <div class="d-flex mb-4 pb-1">
                <span class="me-4 text-primary">
                  <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path>
                  </svg>
                </span>
                <div>
                  <h5>ΜΕΤΑΦΟΡΑ ΠΡΟΣ &amp; ΑΠΟ ΑΕΡΟΔΡΟΜΙΟ</h5>
                  <p class="mb-0 text-muted">Παρέχουμε την άμεση και δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος με ιδιωτικό μας όχημα. Η μεταφορά των μικρών παιδιών έως και 5 ετών γίνεται με τα ειδικά παιδικά καθίσματα.</p>
                </div>
              </div>
              <div class="d-flex">
                <span class="me-4 text-primary">
                  <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 7a2 2 0 012 2m4 0a6 6 0 01-7.743 5.743L11 17H9v2H7v2H4a1 1 0 01-1-1v-2.586a1 1 0 01.293-.707l5.964-5.964A6 6 0 1121 9z"></path>
                  </svg>
                </span>
                <div>
                  <h5> ΜΕΤΑΦΟΡΑ ΑΠΟΣΚΕΥΩΝ</h5>
                  <p class="mb-0 text-muted">Παρέχουμε δωρεάν τη μεταφορά των αποσκευών σας προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος με την κράτησή σας.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>
                    
  <section class="py-5 overflow-hidden">
    <div class="container py-5">
      <div class="row align-items-center">
        <div class="position-relative col-12 col-lg-6 order-last order-lg-first mt-5 mt-lg-0">
          <img class="img-fluid position-relative mx-auto rounded w-100" style="z-index:10" src="https://images.unsplash.com/photo-1603792835233-f4f7c2497d4f?crop=entropy&amp;cs=srgb&amp;fm=jpg&amp;ixid=M3wzMzIzMzB8MHwxfHNlYXJjaHwyfHx0cmF2ZWxlciUyMGluJTIwYWlycG9ydHxlbnwwfDF8fHwxNzAyOTMxMDg3fDA&amp;ixlib=rb-4.0.3&amp;q=85&amp;w=1920" alt="">
          <img class="img-fluid position-absolute" width="160" height="160" style="top:0; left:0; margin-left: -48px; margin-top: -48px;" src="{{ asset('front/metis-assets/elements/blob-tear.svg')}}" alt="">
          <img class="img-fluid position-absolute" width="160" height="160" style="right:0; bottom:0; margin-right: -48px; margin-bottom: -48px;" src="{{ asset('front/metis-assets/elements/blob-tear.svg')}}" alt="">
        </div>
        <div class="col-12 col-lg-6 py-5">
          <div class="row">
            <div class="col-12 col-lg-8 mx-auto">
              <span class="badge rounded-pill bg-primary">Υπηρεσίες</span>
              <h2 class="mt-3 mb-5 fs-1 fw-bold">Κάντε το ταξίδι σας ευκολότερο</h2>
              <div class="d-flex mb-4 pb-1">
                <span class="me-4 text-primary">
                  <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M8 9l3 3-3 3m5 0h3M5 20h14a2 2 0 002-2V6a2 2 0 00-2-2H5a2 2 0 00-2 2v12a2 2 0 002 2z"></path>
                  </svg>
                </span>
                <div>
                  <h5>ΚΟΥΚΟΥΛΑ ΟΧΗΜΑΤΟΣ</h5>
                  <p class="mb-0 text-muted">Για επιπλέον προστασία του οχήματός σας, προσφέρουμε κουκούλα για κάθε τύπο αυτοκινήτου</p>
                </div>
              </div>
              <div class="d-flex mb-4 pb-1">
                <span class="me-4 text-primary">
                  <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path>
                  </svg>
                </span>
                <div>
                  <h5>ΜΕΤΑΦΟΡΑ ΠΑΙΔΙΩΝ</h5>
                  <p class="mb-0 text-muted">Η μεταφορά παιδιών ηλικίας ως 5 ετών γίνεται με ασφάλεια με την χρήση ειδικών παιδικών καθισμάτων, κατόπιν ενημέρωσής σας.</p>
                </div>
              </div>
              <div class="d-flex">
                <span class="me-4 text-primary">
                  <svg width="32" height="32" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg">
                    <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M15 7a2 2 0 012 2m4 0a6 6 0 01-7.743 5.743L11 17H9v2H7v2H4a1 1 0 01-1-1v-2.586a1 1 0 01.293-.707l5.964-5.964A6 6 0 1121 9z"></path>
                  </svg>
                </span>
                <div>
                  <h5>ΕΠΙΛΟΓΗ ΚΛΕΙΔΙΩΝ</h5>
                  <p class="mb-0 text-muted">Μπορείτε να επιλέξετε αν θα μας αφήσετε τα κλειδιά του οχήματός σας ή εάν θα τα πάρετε μαζί στο ταξίδι σας.</p>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </section>

  @include('front.partials.newsletter')
    
@endsection