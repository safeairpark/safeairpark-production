@extends('front.layouts.layout')

@section('extra-head')
 
    <title>Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος | SafeAirPark</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ" name="keywords"> 
@endsection

@section('content')

<section class="position-relative" style="margin-top:15px;"><div class="d-none d-lg-block position-absolute top-0 end-0 bottom-0 start-0 col-6 ms-auto bg-light"></div>
<div class="container">
    <div class="row">
      <div class="col-12 col-md-8 col-lg-6 mx-auto text-center text-lg-start">
        <div class="col-lg-10 pt-lg-5 pb-4">
          <h2 class="mb-3 fs-1 fw-bold">
            <span>Το πιο</span>
            <span class="text-primary">ασφαλές</span>
            <span>και</span>
            <span class="text-primary">οικονομικό</span>
            <span>parking αεροδρομίου</span>
          </h2>
          <p class="pe-lg-5 text-muted mb-0">Η safeairpark είναι ο ασφαλής και οικονομικός χώρος φύλαξης αυτοκινήτων για το αεροδρόμιο Ελ. Βενιζέλος, που θα κάνει το ταξίδι σας ευκολότερο.</p>
        </div>
        <div>
          {{-- 
          <a class="btn btn-primary me-2" href="{{ route('front.booking')}}">ΚΑΝΕ ΚΡΑΤΗΣΗ</a>
          <a class="btn btn-outline-primary" href="{{ route('front.pricelist')}}">ΤΙΜΟΚΑΤΑΛΟΓΟΣ</a>
          <a type="button" href="{{ route('front.booking')}}" class="btn btn-primary btn-lg">ΤΗΛΕΦΩΝΙΚΗ ΚΡΑΤΗΣΗ | 6932722710</a>
          --}}
          <a type="button" href="{{ route('front.booking')}}" id="btn-form-booking" class=" btn btn-primary btn-lg">ΦΟΡΜΑ ΚΡΑΤΗΣΗΣ</a>
          <a type="button" href="tel:6932722710" id="btn-phone-booking" class=" btn btn-secondary btn-lg">ΤΗΛΕΦΩΝΙΚΗ ΚΡΑΤΗΣΗ 
              <img style="transform: rotate(47deg); margin-top: -3px;" width="20" height="20" src="https://img.icons8.com/ios-filled/50/FFFFFF/phone.png" alt="phone"> 
              6932722710
          </a>
        </div>
      </div>
      <div class="col-12 col-lg-6 position-relative">
        <div>
          <img class="img-fluid" src="{{asset('front/metis-assets/illustrations/working-from-airport.png')}}" alt=""></div>
      </div>
    </div>
  </div>
</section>

@php $intersect_image = asset('front/metis-assets/backgrounds/intersect.svg');@endphp
<section class="py-5" style="background-size: contain; background-repeat: no-repeat; background-image: url('{{$intersect_image}}');"><div class="container">
    <div class="row">
      <div class="col-12 col-lg-6 d-flex">
        <div class="my-auto">
          <h2 class="mb-4 fs-1 fw-bold">
            Ασφαλής φύλαξη αυτοκινήτου όλο το
            <span class="text-primary">24ώρο</span>
            στο Αεροδρόμιο Ελ. Βενιζέλος
          </h2>
          <p class="mb-4 text-muted">Εξυπηρετούμε πλήρως τις ανάγκες του επιβατικού κοινού, προσφέροντας οικονομική, ασφαλή και ποιοτική φύλαξη του αυτοκινήτου σας, και παράλληλα άρτια οργάνωση του ταξιδιού σας, με φροντίδα και υπηρεσίες που απλά … θα σας διευκολύνουν!</p>
          <a class="btn btn-primary" href="{{ route('front.company')}}">Η ΕΤΑΙΡΕΙΑ</a>
        </div>
      </div>
      <div class="col-12 col-lg-6 mt-5 mt-lg-0 pt-3 pt-lg-0">
        <ul class="px-lg-5 list-unstyled">
<li class="d-flex mb-5">
            <div class="pe-4">
              <span class="d-flex align-items-center justify-content-center text-primary rounded-circle fs-3 bg-dark text-light" style="width: 64px; height: 64px;">1</span>
            </div>
            <div>
              <h4 class="fw-bold">ΦΥΛΑΞΗ ΟΧΗΜΑΤΟΣ</h4>
              <p class="text-muted">Είμαστε στη διάθεσή σας όλο το 24ωρο, με συνεχή και φιλική εξυπηρέτηση. Ο χώρος μας φυλάσσεται συνεχώς και το όχημά σας είναι ασφαλές καθώς επιβλέπεται από ηλεκτρονικά μέσα αλλά και από το έμπειρο προσωπικό μας.</p>
            </div>
          </li>
          <li class="d-flex mb-5">
            <div class="pe-4">
              <span class="d-flex align-items-center justify-content-center rounded-circle fs-3 bg-dark text-light" style="width: 64px; height: 64px;">2</span>
            </div>
            <div>
              <h4 class="fw-bold">ΔΩΡΕΑΝ ΜΕΤΑΦΟΡΑ ΠΡΟΣ &amp; ΑΠΟ ΑΕΡΟΔΡΟΜΙΟ</h4>
              <p class="text-muted">Παρέχουμε την άμεση και δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος με ιδιωτικό μας όχημα. Η μεταφορά των μικρών παιδιών έως και 5 ετών γίνεται με τα ειδικά παιδικά καθίσματα.</p>
            </div>
          </li>
          <li class="d-flex">
            <div class="pe-4">
              <span class="d-flex align-items-center justify-content-center rounded-circle fs-3 bg-dark text-light" style="width: 64px; height: 64px;">3</span>
            </div>
            <div>
              <h4 class="fw-bold">ΜΕΤΑΦΟΡΑ ΑΠΟΣΚΕΥΩΝ</h4>
              <p class="text-muted">Παρέχουμε δωρεάν τη μεταφορά των αποσκευών σας προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος με την κράτησή σας.</p>
            </div>
          </li>
        </ul>
</div>
    </div>
  </div>
</section>


<section class="py-5 bg-light"><div class="container">
    <div class="row d-flex align-items-center mb-4">
      <div class="col-12 col-lg-6">
        <h2 class="mb-4 fs-1 fw-bold">
          <span>Κάντε το ταξίδι σας</span>
          <span class="text-primary">ευκολότερο</span>
          <span>με τις επιπλέον υπηρεσίες μας</span>
        </h2>
      </div>
      <div class="col-12 col-lg-5">
        <p class="text-muted">Για την καλύτερη εξυπηρέτησή σας, προσφέρουμε ένα πλήρες πακέτο υπηρεσιών που θα διευκολύνουν την οργάνωση του ταξιδιού σας.</p>
        <a class="btn btn-primary me-2" href="{{ route('front.services')}}">ΥΠΗΡΕΣΙΕΣ</a>
        {{-- <a class="btn btn-outline-primary" href="{{ route('front.services')}}">ΥΠΗΡΕΣΙΕΣ</a>--}}
      </div>
    </div>
    <div class="row">
      <div class=" col-12 col-md-6 col-lg-3 mb-4">
        <div class="py-5 px-4 bg-white rounded shadow-sm text-center">
          <span class="text-primary">
            <svg width="40" height="40" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M4 5a1 1 0 011-1h14a1 1 0 011 1v2a1 1 0 01-1 1H5a1 1 0 01-1-1V5zM4 13a1 1 0 011-1h6a1 1 0 011 1v6a1 1 0 01-1 1H5a1 1 0 01-1-1v-6zM16 13a1 1 0 011-1h2a1 1 0 011 1v6a1 1 0 01-1 1h-2a1 1 0 01-1-1v-6z"></path></svg></span>
          <h6 class="fw-bold my-3">Κουκούλα Οχήματος</h6>
          <p class="text-muted home-service-col">Για επιπλέον προστασία του οχήματός σας, προσφέρουμε κουκούλα για κάθε τύπο αυτοκινήτου, με μόνο 1 ευρώ την ημέρα.</p>
        </div>
      </div>
      <div class="home-service-col col-12 col-md-6 col-lg-3 mb-4">
        <div class="py-5 px-4 bg-white rounded shadow-sm text-center">
          <span class="text-primary">
            <svg width="40" height="40" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M10 6H5a2 2 0 00-2 2v9a2 2 0 002 2h14a2 2 0 002-2V8a2 2 0 00-2-2h-5m-4 0V5a2 2 0 114 0v1m-4 0a2 2 0 104 0m-5 8a2 2 0 100-4 2 2 0 000 4zm0 0c1.306 0 2.417.835 2.83 2M9 14a3.001 3.001 0 00-2.83 2M15 11h3m-3 4h2"></path></svg></span>
          <h6 class="fw-bold my-3">Μεταφορά Παιδιών</h6>
          <p class="text-muted home-service-col">Η μεταφορά παιδιών ηλικίας ως 5 ετών γίνεται με ασφάλεια με την χρήση ειδικών παιδικών καθισμάτων, κατόπιν ενημέρωσής σας.</p>
        </div>
      </div>
      <div class="home-service-col col-12 col-md-6 col-lg-3 mb-4">
        <div class="py-5 px-4 bg-white rounded shadow-sm text-center">
          <span class="text-primary">
            <svg width="40" height="40" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M13 10V3L4 14h7v7l9-11h-7z"></path></svg></span>
          <h6 class="fw-bold my-3">Επιλογή Κλειδιών</h6>
          <p class="text-muted home-service-col">Μπορείτε να επιλέξετε αν θα μας αφήσετε τα κλειδιά του οχήματός σας ή εάν θα τα πάρετε μαζί στο ταξίδι σας.</p>
        </div>
      </div>
      <div class="home-service-col col-12 col-md-6 col-lg-3 mb-4">
        <div class="py-5 px-4 bg-white rounded shadow-sm text-center">
          <span class="text-primary">
            <svg width="40" height="40" fill="none" stroke="currentColor" viewbox="0 0 24 24" xmlns="http://www.w3.org/2000/svg"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m5.618-4.016A11.955 11.955 0 0112 2.944a11.955 11.955 0 01-8.618 3.04A12.02 12.02 0 003 9c0 5.591 3.824 10.29 9 11.622 5.176-1.332 9-6.03 9-11.622 0-1.042-.133-2.052-.382-3.016z"></path></svg></span>
          <h6 class="fw-bold my-3">24/7 Καταγραφή Χώρου</h6>
          <p class="text-muted home-service-col">Ελέγχουμε τον χώρο στάθμευσης των οχημάτων σας, 24 ώρες την ημέρα και 7 μέρες την εβδομάδα.</p>
        </div>
      </div>
    </div>
  </div>
</section>


<section class="py-5" style="background-image: url('{{$intersect_image}}'); background-size: contain; background-repeat: no-repeat; background-position: top;"><div class="container">
    <div class="row mb-5">
      <div class="col-12 col-md-8 col-lg-7 mx-auto text-center">
        <h2 class="mb-3 fs-1 fw-bold">
          <lt-highlighter contenteditable="false" style="display: none;" data-lt-linked="1"><lt-div spellcheck="false" class="lt-highlighter__wrapper" style="width: 172.17px !important; height: 53.0909px !important; transform: none !important; transform-origin: 0px 0px !important; zoom: 1 !important; margin-top: -2.90909px !important; margin-left: 134.341px !important;"><lt-div class="lt-highlighter__scroll-element" style="top: 0px !important; left: 0px !important; width: 172px !important; height: 53px !important;"></lt-div></lt-div></lt-highlighter><span contenteditable="false" data-lt-tmp-id="lt-704343" spellcheck="false" data-gramm="false">Κερδίστε</span>
          <span class="text-primary">χρόνο</span>
          <span>και</span>
          <span class="text-primary">χρήμα</span>
        </h2>
        <p class="mb-0 text-muted fs-4">Οι καλύτερες τιμές της αγοράς</p>
      </div>
    </div>
    <div class="row">
      <div class="col-12 col-md-6 col-lg-6 mb-6">
        <div class="px-3 py-5 text-center bg-white rounded shadow-sm">
          <img class="img-fluid mx-auto" width="75" height="75" src="{{asset('front/images/theme/car.png')}}" alt=""><h3 class="fw-bold">Αυτοκίνητο</h3>
          <span class="fs-2 fw-bold ">από</span>
          <span class="fs-1 fw-bold text-primary">9€ έως 2€</span>
          <span class="fs-2 fw-bold ">την ημέρα</span>
          <!-- <p class="mt-1 text-muted">&tau;&eta;&nu; &eta;&mu;&#941;&rho;&alpha;</p> -->
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0">
<li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul>
</div>
          <div>
            <a class="btn btn-primary me-2" href="{{ route('front.pricelist')}}">ΤΙΜΟΚΑΤΑΛΟΓΟΣ</a>
            <!-- <a class="btn btn-outline-secondary" href="#">Purchase</a>-->
          </div>
        </div>
      </div>
      <!-- <div class="col-12 col-md-6 col-lg-4 mb-4"> <div class="px-3 py-5 text-center text-white bg-dark rounded shadow-sm"> <img class="mb-4 img-fluid mx-auto" width="75" height="75" src="metis-assets/illustrations/job-interview.png" alt=""> <h3 class="mb-2 fs-1 fw-bold">Intermediate</h3> <span class="fs-1 fw-bold">$55.99</span> <p class="mt-1">user per month</p> <div class="my-4 py-2 d-flex justify-content-center"> <ul class="list-unstyled text-start mb-0"> <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>6 Emails</span> </li> <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>4 Datebases</span> </li> <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>Unlimited Domains</span> </li> <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>35 GB Storage</span> </li> </ul> </div> <div><a class="btn btn-light me-2" href="#">Start Free Trial</a><a class="btn btn-outline-light" href="#">Purchase</a></div> </div> </div> -->
      <div class="col-12 col-lg-6 mb-6">
        <div class="px-3 py-5 text-center bg-white rounded shadow-sm">
          <img class="img-fluid mx-auto" width="75" height="75" src="{{asset('front/images/theme/dirt-bike.png')}}" alt=""><h3 class="fw-bold">Μηχανή</h3>
         
          <span class="fs-2 fw-bold ">από</span>
          <span class="fs-1 fw-bold text-primary">7€ έως 2€</span>
          <span class="fs-2 fw-bold ">την ημέρα</span>

          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0">
<li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>50 GB Storage</span> </li> -->
            </ul>
</div>
          <div><a class="btn btn-primary me-2" href="{{ route('front.pricelist')}}">ΤΙΜΟΚΑΤΑΛΟΓΟΣ</a></div>
        </div>
      </div>
    </div>
  </div>
</section>

@include('front.partials.testimonials')
@include('front.partials.newsletter')
    
@endsection