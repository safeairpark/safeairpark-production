@extends('front.layouts.layout')

@section('extra-head')
 
    <title>Η Εταιρεία | Safeairpark Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ"" name="keywords"> 
	
	
@endsection

@section('content')

<section class="py-5"><div class="container">
    <h2 class="mb-4 fw-bold">Η εταιρεία μας</h2>
    <div class="row mb-5">
      <div class="col-12 col-lg-6 py-4 pr-lg-5">
        <span class="badge bg-primary rounded-pill"></span>
        <h4 class="my-4 fw-bold"></h4>
        <p class="mb-4 text-muted">Τώρα πια το πάρκινγκ στο αεροδρόμιο είναι εύκολη υπόθεση! Η safeairpark διαθέτει μια πλήρη γκάμα υπηρεσιών που περιλαμβάνουν εκτός εύκολο, οικονομικό και ασφαλές parking, υπηρεσίες μεταφοράς από και προς το αεροδρόμιο, για την καλύτερη εξυπηρέτηση του ταξιδιώτη του αεροδρόμιου Ελ.Βενιζέλος. Mε την safeairpark, τώρα πια μπορείτε να κάνετε το ταξίδι σας εύκολα και ευχάριστα, χωρίς να έχετε έγνοια για το πάρκινγκ του αυτοκινήτου σας ή την μεταφορά σας.</p>
      </div>
      <div class="col-12 col-lg-6">
        <img class="w-100 rounded" style="object-fit: cover; height: 20rem;" src="{{asset('front/images/theme/safeairpark-3-small.jpg')}}" alt=""></div>
    </div>
    <div class="row mb-5">
      <div class="col-12 col-lg-6 py-4 order-lg-1 pl-lg-5">
        <span class="badge bg-primary rounded-pill"></span>
        <h4 class="my-4 fw-bold"></h4>
        <p class="mb-4 text-muted">Η safeairpark είναι ένας νέος χώρος φύλαξης αυτοκινήτων για το αεροδρόμιο Ελ. Βενιζέλος, που θα κάνει το ταξίδι σας ευκολότερο. Εξυπηρετούμε πλήρως τις ανάγκες του επιβατικού κοινού, προσφέροντας ολοκληρωμένες υπηρεσίες parking για όσους κινούνται από, προς και κοντά στο αεροδρόμιο, λύσεις valet parking καθώς και οικονομική, ασφαλή και ποιοτική φύλαξη του αυτοκινήτου σας και παράλληλα άρτια οργάνωση του ταξιδιού σας, με φροντίδα και υπηρεσίες που απλά… θα σας διευκολύνουν!</p>
      </div>
      <div class="col-12 col-lg-6 order-lg-0">
        <img class="w-100 rounded" style="object-fit: cover; height: 20rem;" src="{{asset('front/images/theme/safeairpark-4-small.jpg')}}" alt=""></div>
    </div>
    <div class="row mb-5">
      <div class="col-12 col-lg-6 py-4 pr-lg-5">
        <span class="badge bg-primary rounded-pill"></span>
        <h4 class="my-4 fw-bold"></h4>
        <p class="mb-4 text-muted">Με την safeairpark , το πάρκινγκ στο αεροδρόμιο γίνεται εύκολη υπόθεση! Είτε επιθυμείτε στάθμευση μακράς ή μικρής διάρκειας η safeairpark σας προσφέρει 24ωρη φύλαξη σίγουρα με ασφάλεια αλλά και οικονομικά! Προσφέρουμε τις χαμηλότερες τιμές για υπηρεσίες parking του αεροδρομίου. Καλέστε μας σήμερα για να ενημερωθείτε για τις τιμές των υπηρεσιών μας αλλά και τις τρέχουσες προσφορές μας. Επιπλέον παρέχουμε την άμεση μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος με ιδιωτικό μας όχημα. Η μεταφορά των μικρών παιδιών έως και 5 ετών γίνεται με τα ειδικά παιδικά καθίσματα. Είμαστε στη διάθεσή σας όλο το 24ωρο, με συνεχή και φιλική εξυπηρέτηση. Ο χώρος μας φυλάσσεται συνεχώς και το όχημά σας είναι ασφαλές καθώς επιβλέπεται από ηλεκτρονικά μέσα αλλά και από το έμπειρο προσωπικό μας.</p>
       
      </div>
      <div class="col-12 col-lg-6">
        <img class="w-100 rounded" style="object-fit: cover; height: 20rem;" src="{{asset('front/images/theme/safeairpark-2-small.jpg')}}" alt=""></div>
    </div>
    <!-- <div class="row mb-5"> <div class="col-12 col-lg-6 py-4 order-lg-1 pl-lg-5"> <span class="badge bg-primary rounded-pill">Startup</span> <h4 class="my-4 fw-bold">Curabitur vestibulum odio maximus.</h4> <p class="mb-4 text-muted lh-lg">Aenean tempus orci eu est ultrices hendrerit. Fusce suscipit, leo a semper venenatis, felis urna pulvinar nibh, vitae porta erat risus sed mauris. Vestibulum vehicula leo eget libero eleifend, quis dictum eros bibendum. Maecenas convallis tempor varius.</p> </div> <div class="col-12 col-lg-6 order-0"> <img class="w-100 rounded" style="object-fit: cover; height: 20rem;" src="https://images.unsplash.com/photo-1603665301175-57ba46f392bf?ixlib=rb-1.2.1&ixid=eyJhcHBfaWQiOjEyMDd9&auto=format&fit=crop&w=334&q=80" alt=""> </div> </div> -->
    <!-- <div class="text-center"><a class="btn btn-primary" href="#">Show all posts</a></div> -->
  </div>
</section>


@include('front.partials.testimonials')    
@include('front.partials.newsletter')

@endsection