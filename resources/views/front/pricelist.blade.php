@extends('front.layouts.layout')

@section('extra-head')
 
    <title>Τιμοκατάλογος | Safeairpark Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ" name="keywords"> 
	
@endsection



@section('content')


@php $intersect_image = asset('front/metis-assets/backgrounds/intersect.svg');@endphp

@foreach($pricelists as $pricelist )

<section class="py-5" style="background-image: url('{{$intersect_image}}'); background-size: contain; background-repeat: no-repeat; background-position: top;"><div class="container">
    <div class="row mb-5">
      <div class="col-12 col-lg-6 mx-auto text-center">
        <img class="img-fluid" src="{{asset('front/images/theme/'.$pricelist->icon.'')}}" alt=""><p class="fw-bold fs-2">{{$pricelist->label}}</p>
        <p class="text-muted mb-0 fs-4">Προσφέρουμε 5 ώρες επιπλέον δωρεάν από την συμπλήρωση του τελευταίου 24ωρου χωρίς επιπλέον χρέωση.</p>
      </div>
    </div>
    <div class="row mb-4">
      @foreach($pricelist->prices()->where('status',1)->orderBy('sort','ASC')->get() as $price)
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
             
              <h5 class="mb-1 fw-bold fs-3">{{round( $price->price,2)}}€</h5>
              
              <p class="text-primary fw-bold">
                @if($price->days_min == $price->days_max )
                  @if($price->days_min == 1)
                    ΓΙΑ {{round($price->days_max,2)}} ΗΜΕΡΑ
                  @else
                    ΓΙΑ {{round($price->days_max,2)}} ΗΜΕΡΕΣ
                  @endif
                @endif

                @if($price->days_min < $price->days_max )
                ΓΙΑ {{round($price->days_min,2)}} - {{round($price->days_max,2)}} ΗΜΕΡΕΣ
                @endif
               </p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      @endforeach

      
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">+{{round($pricelist->price_per_day,2)}}€ / ΗΜΕΡΑ</h5>
              <p class="text-primary fw-bold">ΓΙΑ {{round($pricelist->price_after_days,0)}} ΗΜΕΡΕΣ ΚΑΙ ΑΝΩ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>

    </div>
    <div class="text-center">
      <p class="text-muted mb-0 fs-4">Οι τιμές περιλαμβάνουν ΦΠΑ 24%.</p>
      <!--
      <a class="btn btn-primary" href="#">Show more Testimonials</a>
        -->
    </div>
  </div>
</section>

@endforeach

{{-- 
<section class="py-5" style="background-image: url({{$intersect_image}}'); background-size: contain; background-repeat: no-repeat; background-position: top;"><div class="container">
    <div class="row mb-5">
      <div class="col-12 col-lg-6 mx-auto text-center">
        <img class="img-fluid" src="{{asset('front/images/theme/dirt-bike.png')}}" alt=""><p class="fw-bold fs-2">Τιμοκατάλογος Μηχανής</p>
        <p class="text-muted mb-0 fs-4">Προσφέρουμε 5 ώρες επιπλέον δωρεάν από την συμπλήρωση του τελευταίου 24ωρου χωρίς επιπλέον χρέωση.</p>
      </div>
    </div>
    <div class="row mb-4">
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">7€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 1 ΗΜΕΡΑ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">9€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 2 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">11€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 3 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">13€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 4 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">15€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 5 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">17€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 6 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">19€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 7 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">21€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 8 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">23€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 9 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">25€</h5>
              <p class="text-primary fw-bold">ΓΙΑ 10 ΗΜΕΡΕΣ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
      <div class="col-12 col-md-6 col-lg-3 mb-4">
        <div class="p-4 bg-white shadow-sm rounded">
          <div class="d-flex">
            <img class="rounded-circle" style="height: 50px; width: 50px; object-fit: cover;" src="{{asset('front/images/theme/airplane-icon.png')}}" alt=""><div class="ms-3">
              <h5 class="mb-1 fw-bold fs-3">+2€ / ΗΜΕΡΑ</h5>
              <p class="text-primary fw-bold">ΓΙΑ 11 ΗΜΕΡΕΣ ΚΑΙ ΑΝΩ</p>
            </div>
          </div>
          <div class="my-4 py-2 d-flex justify-content-center">
            <ul class="list-unstyled text-start text-muted mb-0"><li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά στο Αεροδρόμιο</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>Μεταφορά αποσκευών</span>
              </li>
              <li class="mb-2">
                <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"><path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path></svg><span>24ωρη φύλαξη</span>
              </li>
              <!-- <li class="mb-2"> <svg class="me-2 text-success" width="24" height="24" xmlns="http://www.w3.org/2000/svg" fill="none" viewbox="0 0 24 24" stroke="currentColor"> <path stroke-linecap="round" stroke-linejoin="round" stroke-width="2" d="M9 12l2 2 4-4m6 2a9 9 0 11-18 0 9 9 0 0118 0z"></path> </svg> <span>10 GB Storage</span> </li> -->
            </ul></div>
        </div>
      </div>
    </div>
    <div class="text-center">
      <p class="text-muted mb-0 fs-4">Οι τιμές περιλαμβάνουν ΦΠΑ 24%.</p>
      <!-- <a class="btn btn-primary" href="#">Show more Testimonials</a> -->
    </div>
  </div>
</section>

--}}

@include('front.partials.newsletter')
    
@endsection