@extends('front.layouts.layout')

@section('before-head')
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/5.1.1/css/all.min.css" integrity="sha512-ioRJH7yXnyX+7fXTQEKPULWkMn3CqMcapK0NNtCN8q//sW7ZeVFcbMJ9RvX99TwDg6P8rAH2IqUSt2TLab4Xmw==" crossorigin="anonymous" referrerpolicy="no-referrer" />


@endsection

@section('extra-head')
 
    <title>Billing | Safeairpark Parking στο Αεροδρόμιο Ελευθέριος Βενιζέλος</title>
    <meta content="SafeAirPark Πάρκινγκ στο αεροδρόμιο Ελευθέριος Βενιζέλος. Online κράτηση parking. Φύλαξη αυτοκινήτου. Δωρεάν μεταφορά προς και από το αεροδρόμιο Ελευθέριος Βενιζέλος" name="description">
    <meta content="Parking,  Παρκιν, Πάρκινγκ, Αερόδρομιο, Ελευθέριος Βενιζέλος, easyairpark, Πάρκινγκ Αεροδρομίου, Valet, πάρκινγκ αεροδρόμιο,parking αεροδρομίου,parking αεροδρόμιο,parking αεροδρομίου,πάρκινγκ ελευθέριος βενιζέλος,πάρκινγκ στο αεροδρόμιο,πάρκινγκ βενιζέλος αεροδρόμιο,parking αεροδρομίου κόστος,parking ελ βενιζέλος,πάρκιν στο αεροδρομιο,αεροδρομιο ελ βενιζέλος πάρκινγκ" name="keywords"> 
	<meta name="robots" content="noindex">

@endsection


@section('content')

<section class="py-5 booking-section">
    <div class="container"> 
        <div class="row justify-content-center"> 
            <div class="col-12 col-md-8"> 
            <form method="post" action="{{route('front.billing')}}" class="booking-form billing">
                @csrf 
                    <div class="card">
                    
                        <h5 class="card-header">Στοιχεία Κράτησης 
                            <span class="form-subtitle back-link">
                               {{--  <a href="javascript:history.back()"> ΑΛΛΑΓΗ ΗΜΕΡΟΜΗΝΙΩΝ </a>--}}
                               <a href="{{route('front.booking')}}"> ΑΛΛΑΓΗ ΗΜΕΡΟΜΗΝΙΩΝ </a>
                            </span>
                        </h5>

                        <div class="card-body">
                            <div class="row">
                                
                                <div class="col-6"><span class="form-subtitle">Είσοδος:<span> {{$date_from}} </div>
                                <div class="col-6"><span class="form-subtitle">Έξοδος:<span> {{$date_to}} </div> 
                                <div class="col-6"><span class="form-subtitle">Ημέρες:</span> {{$days}} / {{$space_label}}</div>
                                 
                                <div class="col-6"><span class="form-subtitle">Τιμή Kράτησης:</span> {{round($rental_price,2)}}€</div>
                            
                                </div>

                        </div>    

                    </div>

                    <div class="card">
                        <h6 class="card-header">Προσωπικά Στοιχεία</h6>

                        <div class="card-body">

                            <div class="row">
                                
                                    <div class="form-group col-md-6 mb-4">
                                        <label  class="required">Όνομα <span class="asterisk">*</span></label>
                      
                                        <div class="input-group">
                                            <input type="text" class="form-control "  name="c_firstname" 
                                            value="{{old('c_firstname')}}">
                                              
                                        </div>
                                        @if ($errors->has('c_firstname'))
                                          <span class="text-danger">{{ $errors->first('c_firstname') }}</span>
                                       @endif
                                      
                                    </div>

                                    <div class="form-group col-md-6 mb-4">
                                        <label  class="required">Επίθετο <span class="asterisk">*</span></label>
                      
                                        <div class="input-group">
                                            <input type="text" class="form-control "  name="c_lastname" 
                                            value="{{old('c_lastname')}}">
                                              
                                        </div>
                                        @if ($errors->has('c_lastname'))
                                          <span class="text-danger">{{ $errors->first('c_lastname') }}</span>
                                       @endif
                                      
                                    </div>

                                    <div class="form-group col-md-6 mb-4">
                                        <label  class="required">Email <span class="asterisk">*</span></label>
                      
                                        <div class="input-group">
                                            <input type="text" class="form-control "  name="c_email" 
                                            value="{{old('c_email')}}">
                                              
                                        </div>
                                        @if ($errors->has('c_email'))
                                          <span class="text-danger">{{ $errors->first('c_email') }}</span>
                                       @endif
                                      
                                    </div>

                                    <div class="form-group col-md-6 mb-4">
                                        <label  class="required">Τηλέφωνο<span class="asterisk">*</span></label>
                      
                                        <div class="input-group">
                                            <input type="text" class="form-control "  name="c_phone" 
                                            value="{{old('c_phone')}}">
                                              
                                        </div>
                                        @if ($errors->has('c_phone'))
                                          <span class="text-danger">{{ $errors->first('c_phone') }}</span>
                                       @endif
                                      
                                    </div>
                                
                            </div>
                        </div> 
                    </div>

                    <div class="card">
                        <h6 class="card-header">Στοιχεία Αυτοκινήτου</h6>

                        <div class="card-body">

                            <div class="row">
                                <div class="form-group col-md-4 mb-4">
                                    <label  class="required">Αρ. Πινακίδας</label>
                  
                                    <div class="input-group">
                                        <input type="text" class="form-control"  name="car_plate" 
                                        value="{{old('car_plate')}}">
                                          
                                    </div>
                                    @if ($errors->has('car_plate'))
                                      <span class="text-danger">{{ $errors->first('car_plate') }}</span>
                                   @endif
                                  
                                </div>
                            
                                <div class="form-group col-md-4 mb-4">
                                    <label  class="required">Μάρκα</label>
                  
                                    <div class="input-group">
                                        <input type="text" class="form-control"  name="car_make" 
                                        value="{{old('car_make')}}">
                                          
                                    </div>
                                    @if ($errors->has('car_make'))
                                      <span class="text-danger">{{ $errors->first('car_make') }}</span>
                                   @endif
                                  
                                </div>
                        
                                <div class="form-group col-md-4 mb-4">
                                    <label  class="required">Μοντέλο</label>
                  
                                    <div class="input-group">
                                        <input type="text" class="form-control"  name="car_model" 
                                        value="{{old('car_model')}}">
                                          
                                    </div>
                                    @if ($errors->has('car_model'))
                                      <span class="text-danger">{{ $errors->first('car_model') }}</span>
                                   @endif
                                  
                                </div>
                            </div>
                    

                        </div> 
                    </div>

                    <div class="card">
                        <h6 class="card-header">Σημειώσεις</h6>

                        <div class="card-body">
                            <div class="row">
                                <div class="col-12 col-lg-12 mb-3">
                                    <textarea class="form-control py-2 px-3 border-1 h-100" name="c_notes"  style="resize: none;" type="text" placeholder="Γράψτε τις σημειώσεις σας">{{old('c_notes')}}</textarea>
                                    <span class="text-danger">{{ $errors->first('c_notes') }}</span>
                                  </div>
                            </div>
                        </div>
                    </div>


                    <div class="card">
                        <h6 class="card-header">Σύνολο</h6>

                        <div class="card-body">
                            <div class="row">

                                <div class="form-group col-12 col-md-12 mb-4 mt-4">
                                    <span class="form-price-title">Σύνολο:<span> {{round($total,2)}}€
                                </div>

                                
                                <div class="col-12 col-lg-6 mb-3">
                                    <div class="form-check">
                                      <input class="form-check-input" type="checkbox" name="terms" @if(old('terms')) {{"checked"}}@endif>
                                        <label class="from-check-label" style="width:100%">Συμφωνώ με τούς <a href="{{ route('front.terms') }}" target="_blank">όρους χρήσης</a>.</label>
                                        <span class="text-danger">{{ $errors->first('terms') }}</span>
                        
                                    </div>
                                  </div>
                                  

                                <div class="form-group col-12 col-md-12 mb-4">
                                    <button type="submit" class="btn btn-primary">ΟΛΟΚΛΗΡΩΣΗ ΚΡΑΤΗΣΗΣ</button>
                                </div>
                            </div>
                        </div> 
                    </div>
                </form>
            </div>
        </div>
    </div>
</section>                    


@include('front.partials.newsletter')
    
@endsection


@section('before-footer')

@endsection


@section('after-footer')

@endsection
