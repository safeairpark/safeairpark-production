<?php

return [

    'sender' => [
        'staging_address' => env('STAGING_MAIL_FROM_ADDRESS','admin@example.com'),
        'staging_name' => env('STAGING_MAIL_FROM_NAME', 'Demo Safeairpark'),
        
    ],
];